package com.example.park;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecurityParkdemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecurityParkdemoApplication.class, args);
	}

}
