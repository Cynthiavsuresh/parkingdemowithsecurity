package com.example.park.config;

import java.security.Key;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.function.Function;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.example.park.entity.Employee;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;



@Service
public class JwtService {
	private final String SECRET_KEY="cynthiad98dc38128ce5fe3405f60abc9df2291e966a55bb43438671be36e6eeaa58a93";
	private Claims extractAllClaims(String token) {
		return Jwts.parserBuilder()
				.setSigningKey(getSignInKey())
				.build()
				.parseClaimsJws(token)
				.getBody();
	}

	private Key getSignInKey() {
		byte[] keyBytes=Decoders.BASE64.decode(SECRET_KEY);
		return Keys.hmacShaKeyFor(keyBytes);
	}
	
	//to get all claims
	public <T> T extractClaims(String token,Function<Claims,T> claimResolver){
		final Claims claims=extractAllClaims(token);
		return claimResolver.apply(claims);
	}
	//to extract user name from token
	public String extractUserName(String token) {
		return extractClaims(token, Claims::getSubject);
	}
	//to generate token using user object
	 public String generateToken(Employee employee) {
	        return doGenerateToken(employee.getEmail());
	    }
	    private String doGenerateToken(String subject) {

	        Claims claims = Jwts.claims().setSubject(subject);
	        Instant now = Instant.now();
	        LocalDate expirationDate = LocalDate.now().plusDays(10); // Example: token expires in 10 days
	        Instant expirationInstant = expirationDate.atStartOfDay(ZoneId.systemDefault()).toInstant();
	        return Jwts.builder()
	                .setClaims(claims)
	                .setIssuedAt(Date.from(now))
	                .setExpiration(Date.from(expirationInstant))
	                .signWith(getSignInKey(), SignatureAlgorithm.HS256)
	                .compact();
	    }

	    
	    //to check token valid or not
	    public boolean isTokenValid(String token,UserDetails userDetails) {
	    	final String userEmail=extractUserName(token);
	    	return (userEmail.equals(userDetails.getUsername()) && !isTokenExpired(token));
	    }
	    //to check token is expired or not
	    public boolean isTokenExpired(String token) {
	    	return extractExpirationDate(token).before(new Date());
	    }
	    private Date extractExpirationDate(String token) {
	    	return extractClaims(token, Claims::getExpiration);
	    }
}
