package com.example.park.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration {

	@Autowired
	public AuthenticationProvider authenticationProvider;
	@Autowired
	public JwtAuthenticationFilter jwtAuthFilter;


	@Bean
	public SecurityFilterChain filterChain(HttpSecurity http) throws Exception { 
		return http .csrf(AbstractHttpConfigurer::disable)
				.authorizeHttpRequests(authorizeRequests -> 
				authorizeRequests .requestMatchers("/login","/employee/v3/api-docs/**","/employee/swagger-ui.html").permitAll()
				
				.requestMatchers("/appliedspots").hasAuthority("EMPLOYEE")
				.anyRequest()
				.authenticated() ) 
				.exceptionHandling(authorizeRequests->
				authorizeRequests.accessDeniedHandler(new CustomAccessDeniedHandler()))
				.sessionManagement(sessionManagement -> sessionManagement.sessionCreationPolicy(SessionCreationPolicy.STATELESS) ) 
				.addFilterBefore(jwtAuthFilter, UsernamePasswordAuthenticationFilter.class) 
				.build(); 
		}

}
	
	


