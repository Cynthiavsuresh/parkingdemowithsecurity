package com.example.park.controller;



import java.time.LocalDate;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.park.dto.ViewReportResponse;
import com.example.park.service.AppliedSpotsService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/appliedspots")
@RequiredArgsConstructor
public class AppliedSpotsController {

	private final AppliedSpotsService appliedSpotsService;

	@GetMapping("/date")
	public ResponseEntity<ViewReportResponse> viewReport(@RequestParam("date") LocalDate date,
			@RequestParam(required = false, defaultValue = "0") Integer page,
			@RequestParam(required = false, defaultValue = "2") Integer size) {
		log.info("Report on Applied spots on that date are below");
		ViewReportResponse response = appliedSpotsService.viewReport(date, page, size);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

}
