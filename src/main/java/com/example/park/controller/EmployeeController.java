package com.example.park.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.example.park.dto.CredentialsDto;
import com.example.park.dto.TokenDto;
import com.example.park.service.LoginService;

import lombok.RequiredArgsConstructor;

@RestController
//@RequestMapping("/employees")
@RequiredArgsConstructor
public class EmployeeController {
Logger logger = LoggerFactory.getLogger(getClass());
	
	private final LoginService loginService;
	
	
	/**
	 * This method is used to check the login credentials
	 * @param credentials
	 * @return status message with token
	 */
	@PostMapping(value="/login",consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<TokenDto> checkLogin(@RequestBody CredentialsDto credentials){
		logger.info("inside login checking credentials");
		TokenDto token=loginService.checkCredentials(credentials);
		return new ResponseEntity<>(token,HttpStatus.OK);
	}
	@GetMapping("/check")
	public String check() {
		return "Cynthia";
	}
}
