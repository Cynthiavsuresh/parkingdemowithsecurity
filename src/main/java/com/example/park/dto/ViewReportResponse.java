package com.example.park.dto;



import org.springframework.data.domain.Page;

public record ViewReportResponse(ResponseDto response, Page<ReportResponse>reportResponse) {

}
