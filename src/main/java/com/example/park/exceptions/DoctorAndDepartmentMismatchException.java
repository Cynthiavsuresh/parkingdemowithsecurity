package com.example.park.exceptions;

public class DoctorAndDepartmentMismatchException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DoctorAndDepartmentMismatchException(String message) {
		super(message);
	}

}
