package com.example.park.exceptions;

public class ErrorResponse {
	
	private String errorStatusCode;
	private String errorMessage;
	public String getErrorStatusCode() {
		return errorStatusCode;
	}
	public void setErrorStatusCode(String errorStatusCode) {
		this.errorStatusCode = errorStatusCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public ErrorResponse(String errorStatusCode, String errorMessage) {
		super();
		this.errorStatusCode = errorStatusCode;
		this.errorMessage = errorMessage;
	}
	public ErrorResponse() {
		super();
	}
	
	
}
