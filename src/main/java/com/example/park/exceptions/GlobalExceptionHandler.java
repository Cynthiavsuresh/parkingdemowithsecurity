package com.example.park.exceptions;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.HandlerMethodValidationException;

import com.example.park.dto.ValidationDto;

import jakarta.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class GlobalExceptionHandler {
	@ExceptionHandler(HttpMessageNotReadableException.class)
	public ResponseEntity<Object> messageNotReadableHandler(HttpMessageNotReadableException ex) {
		return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(HandlerMethodValidationException.class)
	public ResponseEntity<ValidationDto> handlerMethodValidationException(HandlerMethodValidationException ex,
			HttpServletRequest request) {
		Map<String, String> errorMap = new HashMap<>();
		
		ValidationDto validationMessage = new ValidationDto();
		validationMessage.setValidationErrors(errorMap);
		validationMessage.setErrorCode("WP011");
		validationMessage.setErrorMessage("please add valid input data");
		return new ResponseEntity<>(validationMessage, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ValidationDto> handllerMethodAurgumentException(MethodArgumentNotValidException ex) {
		Map<String, String> errorMap = new HashMap<>();
		ex.getBindingResult().getFieldErrors()
				.forEach(error -> errorMap.put(error.getField(), error.getDefaultMessage()));
		ValidationDto validationMessage = new ValidationDto();
		validationMessage.setValidationErrors(errorMap);
		validationMessage.setErrorCode("WP011");
		validationMessage.setErrorMessage("please add valid input data");
		return new ResponseEntity<>(validationMessage, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(DoctorNotFoundException.class)
	public ResponseEntity<ErrorResponse> messageNotReadableHandler(DoctorNotFoundException ex) {
		ErrorResponse error=new ErrorResponse("EX1001", ex.getMessage());
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}
	@ExceptionHandler(BadCredentialsException.class)
	public ResponseEntity<ErrorResponse> badCredentialsExceptionHandler(BadCredentialsException ex) {
		ErrorResponse error=new ErrorResponse("EX1004", "please provide valid credentials");
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}
	@ExceptionHandler(SlotNotAvailableException.class)
	public ResponseEntity<ErrorResponse> slotNotAvailableExceptionHandler(SlotNotAvailableException ex) {
		ErrorResponse error=new ErrorResponse("EX1003", ex.getMessage());
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}


	@ExceptionHandler(DoctorAndDepartmentMismatchException.class)
	public ResponseEntity<ErrorResponse> handleDoctorAndDepartmentMismatchException(
			DoctorAndDepartmentMismatchException ex) {
		ErrorResponse errorResponse = new ErrorResponse("EX-1002", ex.getMessage());
		return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.BAD_REQUEST);
	}


	@ExceptionHandler(SlotExpiredException.class)
	public ResponseEntity<ErrorResponse> handleSlotExpiredException(SlotExpiredException ex) {
		ErrorResponse errorResponse = new ErrorResponse("EX-1004", ex.getMessage());
		return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(DepartmentNotFoundException.class)
	public ResponseEntity<ErrorResponse> handleDepartmentNotFoundException(DepartmentNotFoundException ex) {
		ErrorResponse errorResponse = new ErrorResponse("EX-1005", ex.getMessage());
		return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.BAD_REQUEST);
	}

}
