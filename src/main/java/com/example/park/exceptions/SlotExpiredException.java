package com.example.park.exceptions;

public class SlotExpiredException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SlotExpiredException(String message) {
		super(message);
	}

}
