package com.example.park.repository;



import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.park.entity.AppliedSpot;

public interface AppliedSpotsRepository extends JpaRepository<AppliedSpot, Long> {

	List<AppliedSpot> findByDateApplied(LocalDate date);

	//List<AppliedSpot> getByDateApplied(LocalDate date);

}
