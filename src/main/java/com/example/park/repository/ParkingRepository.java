package com.example.park.repository;



import org.springframework.data.jpa.repository.JpaRepository;

import com.example.park.entity.Parking;

public interface ParkingRepository extends JpaRepository<Parking, String> {

}
