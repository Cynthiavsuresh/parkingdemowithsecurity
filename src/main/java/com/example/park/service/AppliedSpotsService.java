package com.example.park.service;



import java.time.LocalDate;

import com.example.park.dto.ViewReportResponse;

public interface AppliedSpotsService {

	ViewReportResponse viewReport(LocalDate date, Integer page, Integer size);

	//ViewReportResponse viewReports(LocalDate date, Integer page, Integer size);

}
