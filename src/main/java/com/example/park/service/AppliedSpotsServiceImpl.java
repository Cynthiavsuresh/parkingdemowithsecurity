package com.example.park.service;



import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.park.dto.ReportResponse;
import com.example.park.dto.ResponseDto;
import com.example.park.dto.ViewReportResponse;
import com.example.park.entity.AppliedSpot;
import com.example.park.exceptions.InvalidDetailsException;
import com.example.park.exceptions.RecordNotFoundException;
import com.example.park.repository.AppliedSpotsRepository;
import com.example.park.utils.ApplicationConstants;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@RequiredArgsConstructor
public class AppliedSpotsServiceImpl implements AppliedSpotsService  {
	
	private final AppliedSpotsRepository appliedSpotsRepository;
	
	@Override
	public ViewReportResponse viewReport(LocalDate date, Integer page, Integer size) {
		List<AppliedSpot> spot =appliedSpotsRepository.findByDateApplied(date);

		if(date.isAfter(LocalDate.now())) {
			throw new InvalidDetailsException(ApplicationConstants.INVALID_DATE_CODE,ApplicationConstants.INVALID_DATE_MESSAGE);
		} 

		log.info("ReportServiceImpl - toViewListOfAppliedReports details");
		Pageable pageable = PageRequest.of(page, size);
		Page<AppliedSpot> reportList = appliedSpotsRepository.findAll(pageable);
		
		if (reportList.isEmpty()) {
			log.warn(ApplicationConstants.NO_RECORD_FOUND_MESSAGE);
			throw new RecordNotFoundException(ApplicationConstants.NO_RECORD_FOUND_CODE,ApplicationConstants.NO_RECORD_FOUND_MESSAGE);
		}
		
		List<ReportResponse> responseList = reportList.getContent().stream()
				.filter(report -> report.getDateApplied().equals(date))
				.map(appliedspot -> {

		ReportResponse reportResponse=new ReportResponse();
		reportResponse.setEmployeeId(appliedspot.getEmployee().getEmployeeId());
		reportResponse.setEmployeeName(appliedspot.getEmployee().getEmployeeName());
		reportResponse.setSpot(appliedspot.getParking().getSpot());
		return reportResponse;
		}).toList();
	

		Page<ReportResponse> pageResponse = new PageImpl<>(responseList);
		
		log.info(ApplicationConstants.SUCCESS_STATUS_MESSAGE);
		return new ViewReportResponse(new ResponseDto(ApplicationConstants.SUCCESS_STATUS_CODE,ApplicationConstants.SUCCESS_STATUS_MESSAGE),
				pageResponse);}
	
	}
	

