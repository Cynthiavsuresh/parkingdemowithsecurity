package com.example.park.service;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

import com.example.park.config.JwtService;
import com.example.park.dto.CredentialsDto;
import com.example.park.dto.TokenDto;
import com.example.park.entity.Employee;
import com.example.park.repository.EmployeeRepository;


@Service
public class LoginServiceImpl implements LoginService {

	Logger logger = LoggerFactory.getLogger(getClass());

	private final EmployeeRepository employeeRepository;
	private final JwtService jwtService;
	private final AuthenticationManager authenticationManager;

	public LoginServiceImpl(EmployeeRepository employeeRepository, JwtService jwtService,
			AuthenticationManager authenticationManager) {
		super();
		this.employeeRepository = employeeRepository;
		this.jwtService = jwtService;
		this.authenticationManager = authenticationManager;
	}

	@Override
	public TokenDto checkCredentials(CredentialsDto credentials) {
		logger.info("inside chcek login credentials service method");
		authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(credentials.getEmail(), credentials.getPassword()));
		Optional<Employee> trainee =employeeRepository.findByEmail(credentials.getEmail());
		String token = jwtService.generateToken(trainee.get());
		TokenDto tokenDto = new TokenDto("200", "successfully generated token", token);
		return tokenDto;
	}

}
