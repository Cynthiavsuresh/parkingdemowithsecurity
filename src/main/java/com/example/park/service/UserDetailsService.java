package com.example.park.service;



import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.park.entity.Employee;
import com.example.park.exceptions.DoctorNotFoundException;
import com.example.park.repository.EmployeeRepository;



@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService{
	@Autowired
	private EmployeeRepository employeeRepo;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Employee employee = employeeRepo.findByEmail(username)
				.orElseThrow(()->new DoctorNotFoundException("Employee not found"));
		Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();
		setAuths.add(new SimpleGrantedAuthority(employee.getRole()));
	    List<GrantedAuthority> Result = new ArrayList<GrantedAuthority>(setAuths);
		return new org.springframework.security.core.userdetails.User(employee.getEmail(), employee.getPassword(),
				Result);
	}

}
