package com.example.park.utils;

public class ApplicationConstants {

	public static final String NO_RECORD_FOUND_MESSAGE = "No Reecord found in this page";
	public static final String NO_RECORD_FOUND_CODE = "204";
	public static final String SUCCESS_STATUS_CODE = "200";
	public static final String SUCCESS_STATUS_MESSAGE = "Successfully Retrived the report";
	public static final String INVALID_DATE_CODE = "403";
	public static final String INVALID_DATE_MESSAGE = "Cannot enter the Funture Date";

}
